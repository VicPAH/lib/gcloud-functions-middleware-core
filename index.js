exports.chainMiddleware = (...funcs) => async function chainMiddleware(req, res) {
  const first = [...funcs].reverse().reduce(
    (next, fn) => async () => fn(req, res, next),
    async () => {},
  );
  return first();
};
