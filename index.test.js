const { chainMiddleware } = require('.');

describe('chainMiddleware()', () => {
  test('handlers called in order', async () => {
    const order = [];
    const s1 = Symbol('1');
    const s2 = Symbol('2');
    const s3 = Symbol('3');
    const f1 = jest.fn(async (req, res, next) => { order.push(s1); await next(); });
    const f2 = jest.fn(async (req, res, next) => { order.push(s2); await next(); });
    const f3 = jest.fn(async (req, res, next) => { order.push(s3); await next(); });
    await chainMiddleware(f1, f2, f3)();
    expect(order).toEqual([s1, s2, s3]);
  });
  test('stops if next isnt called', async () => {
    const f1 = jest.fn(async (req, res, next) => { await next(); });
    const f2 = jest.fn(() => {});
    const f3 = jest.fn(() => {});
    await chainMiddleware(f1, f2, f3)();
    expect(f1).toHaveBeenCalled();
    expect(f2).toHaveBeenCalled();
    expect(f3).not.toHaveBeenCalled();
  });
  test('errors can be caught', async () => {
    let caught = false;
    const expErr = new Error();
    const f1 = jest.fn(async (req, res, next) => {
      try {
        await next();
      } catch (err) {
        expect(err).toBe(expErr);
        caught = true;
      }
    });
    const f2 = jest.fn(() => {
      throw expErr;
    });
    await chainMiddleware(f1, f2)();
    expect(caught).toBe(true);
  });
  test('mutable req/res', async () => {
    const outerReq = { req: true };
    const outerRes = { res: true };
    const sReq = Symbol('req');
    const sRes = Symbol('res');
    const f1 = jest.fn(async (req, res, next) => {
      expect(req).toBe(outerReq);
      expect(res).toBe(outerRes);
      req.f1 = sReq;
      res.f1 = sRes;
      await next();
    });
    const f2 = jest.fn((req, res) => {
      expect(req).toBe(outerReq);
      expect(res).toBe(outerRes);
      expect(req.f1).toBe(sReq);
      expect(res.f1).toBe(sRes);
    });
    await chainMiddleware(f1, f2)(outerReq, outerRes);
    expect(f2).toHaveBeenCalled();
  });
  test('async handlers', async () => {
    const order = [];
    const s1 = Symbol('1');
    const s2 = Symbol('2');
    const s3 = Symbol('3');
    const f1 = jest.fn(async (req, res, next) => {
      order.push(s1);
      await next();
      order.push(s3);
    });
    const f2 = jest.fn((req, res, next) => (
      new Promise(resolve => {
        setTimeout(() => {
          order.push(s2);
          resolve();
        }, 1);
      })
    ));
    await chainMiddleware(f1, f2)();
    expect(order).toEqual([s1, s2, s3]);
  });
  test('async handler exceptions', async () => {
    const order = [];
    const expErr = new Error();
    const s1 = Symbol('1');
    const s2 = Symbol('2');
    const s3 = Symbol('3');
    const f1 = jest.fn(async (req, res, next) => {
      order.push(s1);
      try {
        await next();
      } catch (err) {
        expect(err).toBe(expErr);
        order.push(s3);
      }
    });
    const f2 = jest.fn((req, res, next) => (
      new Promise((resolve, reject) => {
        setTimeout(() => {
          order.push(s2);
          reject(expErr);
        }, 1);
      })
    ));
    await chainMiddleware(f1, f2)();
    expect(order).toEqual([s1, s2, s3]);
  });
});
